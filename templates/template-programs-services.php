<?php 
/*
Template Name: Programs & Services
*/
?>
<?php get_header(); ?>
	<div class="bgcfff">
	
		<script>
		  (function() {
		    var cx = '005253526562383066707:dcahbyc_gwe';
		    var gcse = document.createElement('script');
		    gcse.type = 'text/javascript';
		    gcse.async = true;
		    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
		    var s = document.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(gcse, s);
		  })();
		</script>
		<gcse:search></gcse:search>
		<?php $img = get_field('background_image'); ?>
		<?php
		 if (empty($img)) {
		 	$img = get_field('top_background_image', 'option');
		 }
		?>

		<div class="page-header-wrap 
			<?php 
				if (is_child_page()):
					echo 'page-header-wrap-child';
				elseif(is_grandchild_page()):
					echo 'page-header-wrap-grandchild';
				elseif (!is_grandchild_page() && !is_child_page()):
					echo 'page-header-wrap-big';
				endif; 
			?>" <?php if (!is_grandchild_page()): ?> style="background-image: url('<?php echo $img['url']; ?>');" <?php endif; ?>>
			<div class="page-header
			<?php 
				if (is_child_page()): 
					echo 'bigger-title';
				elseif(is_grandchild_page()):
					echo 'h1';
				endif;
			?>
			">
				<span <?php if (!is_grandchild_page() && !is_child_page()): ?> data-fitter-happier-text <?php endif; ?>><?php the_title(); ?></span>
			</div>
		</div>
		<!-- breadcrumbs -->
		<div class="breadcrumbs">
			<?php 
				if ( function_exists('yoast_breadcrumb') ):
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				endif;
			?>
		</div>
	</div>

	<div class="cf sidebar-main-wrap">
		<div id="sidebar" class="sidebar">
			<div class="sidebar-menu"><?php label_side_nav(); ?></div>
		</div><!-- .sidebar -->

		<div id="content" class="site-content">

			<?php get_template_part( 'inc/content' ); ?>

		</div>
	</div>

<?php get_footer(); ?>
