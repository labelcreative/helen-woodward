<?php 
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>

<div class="form-holder-wrap" id="form-holder">
	<div class="cf hidden-form-wrap">
		<div class="contact-text-wrap sixcol first">
			<h2 class="form-headline mid-title"><?php tf('form_headline'); ?></h2>
			<div class="form-text h4"><?php tf('form_text'); ?></div>
		</div>
		<div class="sixcol last">
			<div class="form-form">
				<?php $shortcode = get_field('form_shortcode'); ?>
				<?php echo do_shortcode($shortcode); ?>
			</div>
		</div>
	</div>		
</div>

<?php get_footer(); ?>
