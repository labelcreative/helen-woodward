<?php 
/*
Template Name: Litter
*/
?>
<?php get_header(); ?>
<?php $img = get_field('background_image'); ?>
<div class="big-blue-hero" style="background-image: url('<?php echo $img['url']; ?>');">
	<div class="mw-960 big-blue-hero-inner">
		<div class="cf big-blue-hero-title mb0">
		  <a class="litter-title-twitter-anchor" href="https://twitter.com/followthelitter" target="_blank">
			<span class="litter-title-twitter-icon"><?php svg_options_embed('litter_icon'); ?></span><span><div class="white twitter-title" data-fitter-happier-text><?php tf('title'); ?></div></span>
		  </a>	
		</div>
		<div class="big-blue-hero-content"><?php tf('content'); ?></div>
	</div>
</div>
<div class="breadcrumbs">
	<?php 
		if ( function_exists('yoast_breadcrumb') ):
			yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		endif;
	?>
</div>
<?php $title = get_field('current_title'); ?>
<?php $post_object = get_field('current_litter'); ?>
<?php if( $post_object ): 

	// override $post
	$post = $post_object;
	setup_postdata( $post ); 

	?>

<div class="cf follow-the-litter-featured">
	<?php $img = get_field('image'); ?>
	<div class="follow-the-litter-featured-image matcher" style="background-image: url('<?php echo $img['url']; ?>');"></div>
	<div class="follow-the-litter-featured-content matcher">
		<div class="follow-the-litter-featured-content-inner">
			<h2 class="ttu"><a href="<?php the_permalink(); ?>" class="white"><?php the_title(); ?></a></h2>
			<div class="follow-the-litter-featured-description white"><?php tf('description'); ?></div>
			<?php arrow_button(get_permalink(), 'See the whole story', 'white'); ?> 
		</div>
	</div>
</div>

<?php wp_reset_postdata(); ?>
<?php endif; ?>


<h1 class="biggest-title light-blue text-center"><?php tf('past_title'); ?></h1>
<div class="success-stories follow-the-litter-archive-list">
	<?php 
		$args = array( 
			'post_type' => 'litter', 
			'posts_per_page' => 24 );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post();
	?>
		<div class="success-story">
			<?php
				if ( has_post_thumbnail() ):
					$thumb_id  = get_post_thumbnail_id();
					$thumb_url = wp_get_attachment_image_src($thumb_id,'success-thumb', true);
					$img = $thumb_url[0];
				else:
					$img = 'http://placehold.it/320x270';
				endif;
			?>
			<div class="success-story-image-wrap">
				<a href="<?php the_permalink(); ?>"><img src="<?php echo $img ?>" alt="<?php the_title(); ?>"></a>
			</div>
			<div class="success-story-content-wrap">
				<h1 class="h4 white"><a href="<?php the_permalink(); ?>" class="white"><?php the_title(); ?></a></h1>
				<p class=""><?php tf('short_description'); ?></p>
				<?php arrow_button(get_permalink(), 'Read More', 'white'); ?>
			</div>
			
		</div>
	<?php endwhile; ?>
	<!-- this is going to be a ajax repeater -->
	<?php echo do_shortcode('
	[ajax_load_more 
	repeater="template_1" 
	post_type="litter" 
	offset="12" 
	posts_per_page="12" 
	pause="true" 
	button_label="Load More" 
	images_loaded="true"
	scroll="false"
	]'); ?>
</div>


<?php get_footer(); ?>
