<?php 
/*
Template Name: Adopt A Pet
*/
?>
<?php get_header(); ?>
<div class="bgcfff">

<script>
  (function() {
    var cx = '005253526562383066707:dcahbyc_gwe';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

<?php $img = get_field('top_background_image', 'option'); ?>
<div class="page-header-wrap page-header-wrap-grandchild" style="background-image: url('<?php echo $img['url']; ?>');">
	<h1 class="page-header extra-large-title">
		<span><?php the_title(); ?></span>
	</h1>
</div>
<!-- breadcrumbs -->
<div class="breadcrumbs">
	<?php 
		if ( function_exists('yoast_breadcrumb') ):
			yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		endif;
	?>
</div>
</div>

<div id="content" class="site-content-adopt">

	<?php get_template_part( 'inc/content' ); ?>

</div>
</div>
<?php get_footer(); ?>

