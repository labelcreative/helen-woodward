<?php 
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

<?php 
	$img = get_field('background_image');
?>
<style>
	.video-hero:before{
		background-image: url('<?php echo $img['url']; ?>');
	}
</style>

<div class="video-hero home-hero">
	<div class="video-holder from-b-up video-wrap"  id="video-wrap">
		<div class="video-cover"></div>
		<?php $video = get_field('video'); ?>
		<?php if (!empty($video)): ?>
			<video id="video-hero" class="" height="100%" muted="muted" width="auto" preload="auto" autoplay loop> 
	            <source src="<?php tf('video'); ?>" type="video/mp4">
	        </video>
	    <?php endif; ?>
	</div>
	<div class="cell home-hero-text-sections">
		<?php $i = 1; ?>
		<?php while (have_rows('text')): the_row(); ?>
			<div class="home-hero-text-section home-hero-text-section-<?php echo $i; ?>">
				<h2 class="home-hero-text-section-title"><?php tsf('title'); ?></h2>
				<h2 class="home-hero-text-section-number"><?php tsf('number'); ?></h2>
				<h3 class="h4 home-hero-text-section-number-description"><?php tsf('number_description'); ?></h3>
				<?php arrow_button(get_sub_field('button_link'), get_sub_field('button_text'), 'white'); ?>
			</div>
			<?php $i++; ?>
		<?php endwhile; ?>
	</div>	
	<a href="#home-content" class="home-hero-down-arrow roller">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 width="64.609px" height="53.92px" viewBox="0 0 64.609 53.92" enable-background="new 0 0 64.609 53.92" xml:space="preserve">
		<polyline opacity="0.66" fill="none" stroke="#FFFFFF" stroke-width="5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
			2.5,2.5 34.228,22.409 62.109,2.5 "/>
		<polyline opacity="0.73" fill="none" stroke="#FFFFFF" stroke-width="5" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
			2.5,29.93 34.228,49.838 62.109,29.93 "/>
		</svg>
	</a>
</div>

<div class="home-content" id="home-content">
	<?php while(have_rows('sections')): the_row(); ?>
	
		<?php if( get_row_layout() == 'adopt' ): ?> 
	
			<div class="cf home-content-adopt-section-wrap">
					<div class="home-content-adopt-section fourcol first">	
						<h1 class="adopt-headline extra-large-title"><?php the_sub_field('adopt_headline') ?></h1>
						<?php arrow_button(get_sub_field('adopt_button_link'), get_sub_field('adopt_button_text'), 'dark-grey'); ?>
					</div>
					<div class="home-content-adopt-section-right eightcol last">
						<div class="adopt-text small-medium-title"><?php the_sub_field('adopt_text'); ?></div>
					</div>
			</div>
			
			<!-- Adopt Section  -->
	
			<div class="cf">
				<?php 
					$i = 0; 
					while (have_rows('pets')): the_row();
				
						// pet csv url
						$url = get_sub_field('feed_url');
						// convert to array
						$feed = csv_feed($url);
						// pet of the week ID
						$id = get_sub_field('pet_of_the_week_id');
						// 'function' to find pet of the week
						foreach ($feed as $link) {
						    if ($link['AnimalID']==$id) {
						        $result = $link;
						        break;
						    }
						}
						
						if (empty($result)){
							$pet_of_the_week = $feed[0];
						} else {
							$pet_of_the_week = $result;
						}

				?>
					<div class="cf home-adoptable-pet-of-week adoptable-pet-of-week-responsive"
					 style="background-image: url('http://www.animalcenter.org/_images/adoptions/large/<?php echo $pet_of_the_week['AnimalID']; ?>.jpg');">
						<div class="cf home-adoptable-pet-of-week-inner">
							<div class="home-adoptable-pet-of-week-image">
								<h3 class="mid-title pet-of-week-title"><?php tsf('title'); ?></h3>
							</div>
					
							<div class="home-adoptable-pet-of-week-content" >
								<div class="home-adoptable-pet-content">
									<ul>
										<li>
											<span class="mid-title name"><?php echo $pet_of_the_week['AName']; ?></span>
										</li>
										<li>
											<span class="home-span-breed"></span>
											<span class="bigger"><?php echo $pet_of_the_week['Breed']; ?></span>
										</li>
										<li>
											<span class="home-span-sex">Sex: </span>
											<span><?php echo $pet_of_the_week['Sex']; ?></span>
										</li>
										<li>
											<span class="home-span-dob">Estimated DOB: </span>
											<span><?php echo $pet_of_the_week['DOB']; ?></span>
										</li>
										<li>
											<span class="home-span-weight">Weight: </span>
											<span><?php echo $pet_of_the_week['Weight']; ?></span>
										</li>
										<li>
											<span class="homespan-color">Color: </span>
											<span><?php echo $pet_of_the_week['Color']; ?></span>
										</li>
									</ul>
									
									<a class="pet-of-week-button open-popup-link">
										<?php arrow_button(get_sub_field('button_link'), get_sub_field('button_text'), 'white'); ?>
									</a>
					
								</div>
							</div>
						</div>
					</div>
					<?php $i++; ?>
				<?php endwhile; ?>

			</div>
				
		<?php elseif( get_row_layout() == 'events' ): ?>
	
			<div class="cf events-header-wrap">
				<h1 class="events-title extra-large-title"><?php the_sub_field('title'); ?></h1>
				<a class="events-button"><?php arrow_button(get_sub_field('link_link'), get_sub_field('link_text'), 'dark-grey'); ?> </a>
			</div>
	
			<div class="cf events-wrap">
	
				<?php $i =1; ?>
	
				<?php while(have_rows('events')): the_row(); ?>
	
					<?php $img = get_sub_field('events_image'); ?>
	
				    <?php if ($i == 1): ?>
				        <div class="image-wrap eightcol first">
				            <div class="rmt-block" style="background-image: url('<?php echo $img['url']; ?>');">
				    <?php elseif ($i == 2): ?>
				        <div class="cf">
				            <div class="sixcol first surf-block" style="background-image: url('<?php echo $img['url']; ?>');">
				    <?php elseif ($i == 3): ?>
				            <div class="sixcol last holiday-block" style="background-image: url('<?php echo $img['url']; ?>');">
				    <?php elseif ($i == 4): ?>
				            <div class="fourcol last spring-block" style="background-image: url('<?php echo $img['url']; ?>');">
				    <?php endif; ?>
	
						<?php //Content ?>
							<h2 class="image-title-top"><?php tsf('title'); ?></h2>
							<div class="events-image-content-inner">
								<h2 class="image-title mid-title white"><?php tsf('title'); ?></h2>
								<?php if($i == 1 || $i == 4): ?>
									<div class="image-description small-title white"><?php tsf('description'); ?></div>
								<?php endif; ?>
								<a class="image-button small-medium-title"><?php arrow_button(get_sub_field('button_link'), get_sub_field('button_text'), 'orange'); ?> </a>
							</div>
						<?php //End Content ?>
	
				        </div>
				    <?php if ($i == 3): ?>
				        </div></div>
				    <?php endif; ?>
	
				<?php $i++; ?>
				<?php endwhile; ?>
	
			</div>
	
		<?php elseif( get_row_layout() == 'give' ): ?>
			
			<?php $img = get_sub_field('background_image'); ?>
			<div class="content-give-section-wrap-all" style="background-image:url('<?php echo $img['url']; ?>');">
				<h1 class="give-header extra-large-title"><?php the_sub_field('give_header') ?></h1>
				<div class="cf give-section-content-wrap">	

						<?php while(have_rows('give_options')): the_row(); ?>
							
							<div class="give-section-content threecol">	
								<div class="give-images"> <?php acf_sub_image('image'); ?> </div>
								<div class="give-section">
									<p class="h1 amount"><?php the_sub_field('amount') ?></p>
									<div class="give-text small-title"><?php the_sub_field('text') ?></div>
									<a class="give-button" target="_blank"><?php heart_button(get_sub_field('button_link'), get_sub_field('button_text'), 'orange'); ?></a>
								</div>	
							</div>

						<?php endwhile; ?>	
				</div>
	    	</div> 
	
	   <?php endif; ?>
	
	<?php endwhile; ?>
</div>
<?php get_footer(); ?>