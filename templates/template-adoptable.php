<?php 
/*
Template Name: Adoptable Pets
*/
?>
<?php get_header(); ?>
<?php 
	// pet csv url
	$url = get_field('pet_feed_url');
	// convert to array
	$feed = csv_feed($url);
	// pet of the week ID
	$id = get_field('pet_of_the_week_id');
	// 'function' to find pet of the week
	foreach ($feed as $link) {
	    if ($link['AnimalID']==$id) {
	        $result = $link;
	        break;
	    }
	}
	// set pet of the week array to variable
	
	if (empty($result)){
		$pet_of_the_week = $feed[0];
	} else{
		$pet_of_the_week = $result;
	}
?>
<?php $img = get_field('background_image'); ?>
<div class="big-blue-hero" style="background-image: url('<?php echo $img['url']; ?>');">
	<div class="mw-960 big-blue-hero-inner">
		<h1 class="big-blue-hero-title mb0"><?php tf('title') ?></h1>
		<div class="big-blue-hero-content"><?php tf('content'); ?></div>
	</div>
</div>
<div class="breadcrumbs">
	<?php 
		if ( function_exists('yoast_breadcrumb') ):
			yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		endif;
	?>
</div>
<?php  ?>
<div class="cf adoptable-pet-of-week" style="background-image: url('http://www.animalcenter.org/_images/adoptions/large/<?php echo $pet_of_the_week['AnimalID']; ?>.jpg');">
	<div class="cf adoptable-pet-of-week-inner">
		<div class="adoptable-pet-of-week-image matcher sevencol first">
			<div class="adoptable-image-header biggest-title"><?php tf('pet_of_the_week_title'); ?></div>
		</div>
		<div class="adoptable-pet-of-week-content matcher fivecol last" >
			<h3 class="pet-of-week-name"><?php echo $pet_of_the_week['AName']; ?></h3>
			<p class="pet-of-week-description"><?php echo $pet_of_the_week['Desc']; ?></p>
			<a href="#id-<?php echo $pet_of_the_week['AnimalID']; ?>" class="button button-white-blue-border open-popup-link"><?php tf('pet_of_the_week_button_text'); ?></a>
		</div>
	</div>
</div>
<!-- <div class="adoptable-last-updated-wrap text-center">
	<h3 class="blue"><?php tf('updated_time_title'); ?></h3>
	<p><?php //echo $date; ?></p>
</div> -->

	
<div class="cf adoptable-pets p1">
	<?php foreach($feed as $pet):?>
		<?php $video = $pet['VideoFile']; ?>
		<div class="cf adoptable-pet">
			<div class="from-s-up-sixcol from-s-up-first sevencol first adoptable-pet-image <?php if (!empty($video)): echo "adoptable-pet-image-has-video"; endif; ?>">
				<?php if (!empty($video)): ?>
					<a href="https://www.youtube.com/watch?v=<?php echo $video; ?>" class="open-popup-video">
					<svg class="play-button-dog" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				width="153.36px" height="153.36px" viewBox="0 0 153.36 153.36" enable-background="new 0 0 153.36 153.36" xml:space="preserve">
				<circle fill="none" stroke="#FFFFFF" stroke-width="2" stroke-miterlimit="10" cx="76.095" cy="70.394" r="62.247"/>
				<polygon fill="none" stroke="#FFFFFF" stroke-width="2" stroke-miterlimit="10" points="65.596,49.894 65.596,92.894 
				100.595,70.858 "/>
				</svg>

				<?php endif; ?>
				<?php if (!empty($pet['AnimalID'])): ?>
					<img src="http://www.animalcenter.org/_images/adoptions/large/<?php echo $pet['AnimalID']; ?>.jpg" alt="">
				<?php endif; ?>
				<?php if (!empty($video)): ?>
					</a>
				<?php endif; ?>
			</div>
			<div class="from-s-up-sixcol from-s-up-last fivecol last adoptable-pet-content">
				<h3 class="pet-name"><?php echo $pet['AName']; ?></h3>
				<p class="bigger"><?php echo $pet['Breed']; ?></p>
				<ul>
					<li>
						<span class="span-sex">Sex: </span>
						<span><?php echo $pet['Sex']; ?></span>
					</li>
					<li>
						<span class="span-dob">Estimated DOB: </span>
						<span><?php echo $pet['DOB']; ?></span>
					</li>
					<li>
						<span class="span-weight">Weight: </span>
						<span><?php echo $pet['Weight']; ?></span>
					</li>
					<li>
						<span class="span-color">Color: </span>
						<span><?php echo $pet['Color']; ?></span>
					</li>
					<li>
						<span class="span-status">Status: </span>
						<span><?php echo $pet['Status']; ?></span>
					</li>
				</ul>
				<a href="#id-<?php echo $pet['AnimalID']; ?>" class="button open-popup-link">More About <?php echo $pet['AName']; ?></a>
			</div>
		</div>
		<div id="id-<?php echo $pet['AnimalID']; ?>" class="white-popup mfp-hide open-popup-">
			<div class="adoptable-pets--open-inner">
				<?php $video = $pet['VideoFile']; ?>
				<div class="cf adoptable-pet adoptable-pet-lightbox">
					<div class="from-s-up-sixcol from-s-up-first sevencol first adoptable-pet-image">
						<?php if (!empty($pet['AnimalID'])): ?>
							<img src="http://www.animalcenter.org/_images/adoptions/large/<?php echo $pet['AnimalID']; ?>.jpg" alt="">
						<?php endif; ?>
					</div>
					<div class="from-s-up-sixcol from-s-up-last fivecol last adoptable-pet-content-lightbox">
						<h3><?php echo $pet['AName']; ?></h3>
						<p class="adoptable-pet-description"><?php echo $pet['Desc']; ?></p>
						<p class="bigger bigger-lightbox"><?php echo $pet['Breed']; ?></p>
						<ul>
							<li>
								<span>Sex: </span>
								<span><?php echo $pet['Sex']; ?></span>
							</li>
							<li>
								<span>Estimated DOB: </span>
								<span><?php echo $pet['DOB']; ?></span>
							</li>
							<li>
								<span>Weight: </span>
								<span><?php echo $pet['Weight']; ?></span>
							</li>
							<li>
								<span>Color: </span>
								<span><?php echo $pet['Color']; ?></span>
							</li>
							<li>
								<span>Status: </span>
								<span><?php echo $pet['Status']; ?></span>
							</li>
						</ul>
					</div>
				</div>
				<div class="cf adoptable-pet-ctas adoptable-pet-ctas-responsive">
					<?php while(have_rows('content_section')): the_row(); ?>
						<div class="adoptable-pet-cta">
							<h2 class="adopt-headline"><?php tsf('adopt_headline'); ?></h2>
							<p class="adopt-text"><?php tsf('adopt_text'); ?></p>
							<a href="<?php tsf('adopt_button_link'); ?>" class="button button-test"><?php tsf('adopt_button_text'); ?></a>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
		
	<?php endforeach;?>
</div>
<?php // if an id is passed in the url then open that up!
	if (isset($_GET["id"])):
	$id = $_GET["id"]; 
?>
	<script>
		jQuery(window).load(function(){
		jQuery.magnificPopup.open({
		  items: {src: '#id-<?php echo $id; ?>'},type: 'inline'}, 0);
		});
	</script>
<?php endif; ?>
	
<?php get_footer(); ?>



<!-- AnimalID|AName|AType|Weight|Sex|DOB|Breed|Color|Desc|Status|VideoFile

