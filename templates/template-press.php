<?php 
/*
Template Name: Press
*/
?>
<?php get_header(); ?>

<div id="content" class="press-page-wrap">
	<div class="press-header">
		<?php $img = get_field('press_background'); ?>
		<div class="archive-title press-archive-title" style="background-image: url('<?php echo $img['url']; ?>');"><h1 class="inner-archive-title white"><?php the_field('press_header'); ?></h1></div>
	</div>

	<div id="inner-content" class="cf posr inner-content">

			<?php if (have_posts()): ?>
			<div class="inner-content-news">
				<div class="cf press-section-wrap in-the-news" id="in-the-news">
					<h1></h1>
					<?php $subheadline = get_field('in_the_news_subheadline'); ?>
					<?php if (!empty($subheadline)){ ?>
					<p class="subhead press-subhead"><?php echo $subheadline;  ?></p>
					<?php	
					}
					unset($subheadline); ?>
					<?php echo do_shortcode('[ajax_load_more repeater="press" post_type="press" scroll="false"]'); ?>
				</div>	
			</div>
			<?php else : ?>

					<article id="post-not-found" class="hentry clearfix text-center">
						<header class="article-header">
							<h1><?php _e( 'Oops, Post Not Found!', 'labeltheme' ); ?></h1>
						</header>
							<section class="entry-content">
								<p><?php _e( 'Try going back to the homepage.', 'labeltheme' ); ?></p>
						</section>
						<footer class="article-footer">
								<a href="<?php echo site_url(); ?>" class="button">Home</a>
						</footer>
					</article>

			<?php endif; ?>

	</div> <?php // end #inner-content ?>
</div> <?php // end #content ?>
<?php get_footer(); ?>