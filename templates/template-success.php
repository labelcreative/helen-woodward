<?php 
/*
Template Name: Success
*/
?>
<?php get_header(); ?>
<?php $img = get_field('background_image'); ?>
<div class="big-blue-hero" style="background-image: url('<?php echo $img['url']; ?>');">
	<div class="mw-1040 big-blue-hero-inner">
		<h1 class="big-blue-hero-title mb0"><span><?php tf('title') ?></span></h1>
		<div class="big-blue-hero-content"><?php tf('content'); ?></div>
	</div>
</div>
<div class="breadcrumbs">
	<?php 
		if ( function_exists('yoast_breadcrumb') ):
			yoast_breadcrumb('<p id="breadcrumbs">','</p>');
		endif;
	?>
</div>
<div class="cf success-stories success-stories-responsive">
	<?php 
		$args = array( 
			'post_type' => 'success_story', 
			'posts_per_page' => 12 );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post();
	?>
		<div class="success-story">
			<?php
				if ( has_post_thumbnail() ):
					$thumb_id  = get_post_thumbnail_id();
					$thumb_url = wp_get_attachment_image_src($thumb_id,'success-thumb', true);
					$img = $thumb_url[0];
				else:
					$img = 'http://placehold.it/320x270';
				endif;
			?>
			<div class="success-story-image-wrap">
				<a class="success-story-image" href="<?php the_permalink(); ?>"><img src="<?php echo $img ?>" alt="<?php the_title(); ?>"></a>
			</div>
			<div class="success-story-content-wrap">
				<h2 class="h3 m0"><a class="success-name" href="<?php the_permalink(); ?>"><?php tf('name'); ?></a></h2>
				<h1 class="h4 m0"><a class="success-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				<p class="success-description"><?php tf('short_description'); ?></p>
				<?php arrow_button(get_permalink(), 'Read More', 'white'); ?>
			</div>
			
		</div>
	<?php endwhile; ?>
	<!-- this is going to be a ajax repeater -->
	<?php echo do_shortcode('[ajax_load_more repeater="template_1" post_type="success_story" offset="12" posts_per_page="12" pause="true" button_label="Load More"]'); ?>
</div>


<?php get_footer(); ?>
