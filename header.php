<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<?php include 'inc/head.php'; ?>

<body <?php body_class(); ?>>

<?php include 'inc/if-ie.php'; ?>


<div class="page-wrap">

<?php $link = get_field('top_button_link', 'option'); ?>
<?php $text = get_field('top_button_text', 'option'); ?>
	
	<nav role="navigation" class="cf pushy pushy-left top-nav-wrap main-navigation" id="header">
		<div class="nav-bar from-m-down">
			<a href="#" class="menu-btn bt-menu-trigger"><span>Menu</span></a>
			<a href="<?php bloginfo('url'); ?>" class="home-link">
				<?php $img = get_field('logo', 'options'); ?>
				<img src="<?php echo $img['url']; ?>" alt="<?php the_title(); ?>" class="logo-top">
			</a>
			<?php heart_button($link, $text, 'top-nav-button'); ?>
		</div>
		<div class="cf inner-nav-wrap">
			<a href="<?php bloginfo('url'); ?>" class="home-link from-m-up">
				<?php $img = get_field('logo', 'options'); ?>
				<img src="<?php echo $img['url']; ?>" alt="<?php the_title(); ?>" class="logo-top">
			</a>
			<div class="top-menu"><?php label_main_nav(); ?></div>
            
        	
        	<?php heart_button($link, $text, 'top-nav-button from-m-up small-medium-title'); ?>
        </div>
    </nav>
			
    <div class="site-overlay"></div>
		

   	<div id="container" class="cf">
		
		<div class="nav-bar from-m-down nav-bar-outside headroom">
			<a href="#" class="menu-btn bt-menu-trigger"><span>Menu</span></a>
			<a href="<?php bloginfo('url'); ?>" class="home-link">
				<?php $img = get_field('logo', 'options'); ?>
				<img src="<?php echo $img['url']; ?>" alt="<?php the_title(); ?>" class="logo-top">
			</a>
			<?php heart_button($link, $text, 'top-nav-button'); ?>
		</div>




