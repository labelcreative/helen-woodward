<?php get_header(); ?>
<?php $img = get_field('404_image', 'options'); ?>
<div class="cf p1 not-found-header" style="background-image: url('<?php echo $img['url']; ?>');">
	<div class="mw-1140">
		<h1 class="not-found-title white from-s-up-eightcol sevencol"><?php tfo('404_headline') ?></h1>
	</div>
</div>
<div class="mw-1140 p1">
	<h2 class="h1 blue text-center"><?php tfo('404_menu_title'); ?></h2>
</div>
<div class="mw-960 p1 not-found-menu-wrap">
	<?php label_404_menu(); ?>
</div>
<div class="cf p1 mw-1040 adoptable-pet-ctas adoptable-pet-ctas-responsive not-found-ctas">
	<?php while(have_rows('404_ctas', 'options')): the_row(); ?>
		<div class="adoptable-pet-cta adoptable-pet-cta-two matcher">
			<h2 class="m3 adopt-headline"><?php tsf('headline'); ?></h2>
			<p class="adopt-text"><?php tsf('text'); ?></p>
			<a href="<?php tsf('button_link'); ?>" class="button button-test"><?php tsf('button_text'); ?></a>
		</div>
	<?php endwhile; ?>
</div>
<?php get_footer(); ?>
