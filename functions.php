<?php

/*
Author: Label Creative
URL: htp://thelabelcreative.com/

*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/label.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once( 'library/label.php' ); // if you remove this, label will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
require_once( 'library/success-post-type.php' ); 
require_once( 'library/litter-post-type.php' ); 
require_once( 'library/press-post-type.php' ); 
require_once( 'library/news-post-type.php' ); 
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
require_once( 'library/admin.php' ); // this comes turned off by default
/*
4. library/acf.php
	- adding custom acf functions to make things a little easier
*/
require_once( 'library/acf.php' ); 


/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'label-thumb-600', 600, 150, true );
add_image_size( 'label-thumb-300', 300, 100, true );
add_image_size( 'success-thumb', 318, 270, true );
/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'label-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'label-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function label_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'labeltheme' ),
		'description' => __( 'The first (primary) sidebar.', 'labeltheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'labeltheme' ),
		'description' => __( 'The page sidebar.', 'labeltheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar( array(
	'name' => 'Footer Sidebar 1',
	'id' => 'footer-sidebar-1',
	'description' => 'Appears in the footer area',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widgettitle">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => 'Footer Sidebar 2',
	'id' => 'footer-sidebar-2',
	'description' => 'Appears in the footer area',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widgettitle">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => 'Footer Sidebar 3',
	'id' => 'footer-sidebar-3',
	'description' => 'Appears in the footer area',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widgettitle">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => 'Footer Sidebar 4',
	'id' => 'footer-sidebar-4',
	'description' => 'Appears in the footer area',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widgettitle">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => 'Terms and Conditions',
	'id' => 'terms-conditions',
	'description' => 'Appears in the footer area',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widgettitle">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => 'Privacy Policy',
	'id' => 'privacy-policy',
	'description' => 'Appears in the footer area',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widgettitle">',
	'after_title' => '</h3>',
	) );


	// To call the sidebar in your template, you can just copy
	// the sidebar.php file and rename it to your sidebar's name.
	// So using the above example, it would be:
	// sidebar-sidebar2.php


} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/


////a custom excerpt length 

// TRUNCK STRING TO SHORT THE STRINGS
function trunck_string($str = "", $len = 150, $more = 'true') {

    if ($str == "") return $str;
    if (is_array($str)) return $str;
    $str = strip_tags($str);    
    $str = trim($str);
    // if it's les than the size given, then return it

    if (strlen($str) <= $len) return $str;

    // else get that size of text
    $str = substr($str, 0, $len);
    // backtrack to the end of a word
    if ($str != "") {
      // check to see if there are any spaces left
      if (!substr_count($str , " ")) {
        if ($more == 'true') $str .= "...";
        return $str;
      }
      // backtrack
      while(strlen($str) && ($str[strlen($str)-1] != " ")) {
        $str = substr($str, 0, -1);
      }
      $str = substr($str, 0, -1);
      if ($more == 'true') $str;
      if ($more != 'true' and $more != 'false') $str .= $more;
    }
    return $str;
}
// the_content() WITHOUT IMAGES
// GET the_content() BUT EXCLUDE <img> OR <img/> TAGS THEN ECHO the_content()
function custom_excerpt( $Trunckvalue = null ) {
    ob_start();
    the_content();
    $postOutput = preg_replace('/<img[^>]+./','', ob_get_contents());
    ob_end_clean();
    echo trunck_string( $postOutput, $Trunckvalue, true ); ?>
    <?php
}



//// if is a parent of a specific page

// usage
/*
if (is_tree(2)) {
   // stuff
}
*/
function is_tree($pid) {
  global $post;

  $ancestors = get_post_ancestors($post->$pid);
  $root = count($ancestors) - 1;
  $parent = $ancestors[$root];

  if(is_page() && (is_page($pid) || $post->post_parent == $pid || in_array($pid, $ancestors))) {
    return true;
  } else {
    return false;
  }
};



// turn off page comments
// function default_comments_off( $data ) {
//     if( $data['post_type'] == 'page' && $data['post_status'] == 'auto-draft' ) {
//         $data['comment_status'] = 0;
//     } 

//     return $data;
// }
// add_filter( 'wp_insert_post_data', 'default_comments_off' );


// sanitize input, used for making title into a string for the roller ids inside acf
function clean($z){
    $z = strtolower($z);
    $z = preg_replace('/[^a-z0-9 -]+/', '', $z);
    $z = str_replace(' ', '', $z);
    return trim($z, '');
}


// if is 1 level down
function is_child_page() {
	global $post;
	 if ( is_page() && (count(get_post_ancestors($post->ID)) == 1) ) {
	 	return true;
	 }
}
// if is 2 levels down
function is_grandchild_page() {
	global $post;
	 if ( is_page() && (count(get_post_ancestors($post->ID)) >= 2) ) {
	 	return true;
	 }
}

// Function to convert CSV into associative array
function csvToArray($file, $delimiter) { 
  if (($handle = fopen($file, 'r')) !== FALSE) { 
    $i = 0; 
    while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) { 
      for ($j = 0; $j < count($lineArray); $j++) { 
        $arr[$i][$j] = $lineArray[$j]; 
        // $arr[$i][$j] = isset($lineArray[$j]) ? $lineArray[$j] : "";
      } 
      $i++; 
    } 
    fclose($handle); 
  } 
  return $arr; 
} 
// to get the pets csv and create an array
function csv_feed($url){
	$feed = $url;
	// Arrays we'll use later
	$keys = array();
	$newArray = array();
	
	// Do it
	$data = csvToArray($feed, '|');
	// Set number of elements (minus 1 because we shift off the first row)
	$count = count($data) - 1;
	  
	//Use first row for names  
	$labels = array_shift($data);  
	foreach ($labels as $label) {
	  $keys[] = $label;
	}
	// Add Ids, just in case we want them later
	$keys[] = 'id';
	for ($i = 0; $i < $count; $i++) {
	  $data[$i][] = $i;
	}
	  
	// Bring it all together
	for ($j = 0; $j < $count; $j++) {
	  $d = array_combine($keys, $data[$j]);
	  $newArray[$j] = $d;
	}

	return $newArray;
}

// fancy button
function heart_button($link, $text, $class = ""){
	echo '<a href="'.$link.'" class="cf button heart-button '.$class.'">';
		echo '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 width="34.412px" height="29.87px" viewBox="12.457 11.694 34.412 29.87" enable-background="new 12.457 11.694 34.412 29.87"
			 xml:space="preserve">
		<path fill="#FFFFFF" d="M46.869,20.143c0.009-2.893-1.474-8.156-6.599-8.428c-7.564-0.411-10.021,5.483-10.583,7.144
			c-0.557-1.661-3.023-7.555-10.594-7.144c-5.113,0.272-6.646,5.535-6.637,8.428c0.021,7.968,9.325,16.593,17.153,21.327v0.095
			c0-0.016-0.021-0.031,0-0.047c0.029,0.016,0,0.031,0,0.047V41.47C37.439,36.736,46.846,28.111,46.869,20.143z"/>
		</svg>';
		echo '<div><span data-fitter-happier-text>'.$text.'</span></div>';
	echo '</a>';
}
function arrow_button($link, $text, $class = ""){
	echo '<a href="'.$link.'" class="button button-arrow '.$class.'">';
		echo '<span class="arrow-button-right-border"></span>';
		echo '<span class="arrow-button-text">'.$text.'</span>';
	echo '</a>';
}


// button-right-border shortcode
function button_right_border_shortcode($params = array()) {
	extract(shortcode_atts(array(
 		'text' => 'Button Text',
 		'url' => '#',
 		'color' => 'blue'
 	), $params));
	
	return '<a href="'.$url.'" class="button button-right-border ' . $color . '"><span>' . $text . '</span></a>';
}
add_shortcode('button_right_border', 'button_right_border_shortcode');


// button_arrow_shortcode- shortcode
function button_arrow_shortcode($params = array()) {
	extract(shortcode_atts(array(
 		'text' => 'Button Text',
 		'url' => '#',
 		'color' => 'blue'
 	), $params));
	
	return '<a href="'.$url.'" class="button button-arrow ' . $color . '">
		<span class="arrow-button-right-border"></span>
		<span class="arrow-button-text">'.$text.'</span>
	</a>';
}
add_shortcode('button_arrow', 'button_arrow_shortcode');

