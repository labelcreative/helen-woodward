            </div> <?php //end #container ?>
            <footer class="footer" role="contentinfo">

                <div class="cf inner-footer">
                    <div class="cf footer-menus from-l-up-ninecol from-l-up-first">
                        <h3 class="navigate">NAVIGATE</h3>
                        <div id="footer-sidebar1" class="threecol first">
                        <?php
                                if(is_active_sidebar('footer-sidebar-1')){
                                        dynamic_sidebar('footer-sidebar-1');
                                }
                        ?>
                        </div>
                        <div id="footer-sidebar2" class="threecol">
                        <?php
                                if(is_active_sidebar('footer-sidebar-2')){
                                        dynamic_sidebar('footer-sidebar-2');
                                }
                        ?>
                        </div>
                        <div id="footer-sidebar3" class="threecol">
                        <?php
                                if(is_active_sidebar('footer-sidebar-3')){
                                        dynamic_sidebar('footer-sidebar-3');
                                }
                        ?>
                        </div>
                        <div id="footer-sidebar4" class="threecol last">
                        <?php
                                if(is_active_sidebar('footer-sidebar-4')){
                                        dynamic_sidebar('footer-sidebar-4');
                                }
                        ?>
                        </div>
                    </div> 
                    <div class="cf footer-links-wrap from-l-up-threecol from-l-up-last">
                        <div class="social-links">
                            <h3 class="social-title">SOCIAL</h3>
                            <ul>
                                <?php while(have_rows('social_links', 'options')): the_row(); ?>
                                  
                                    <a class="social-links-content-inner" href="<?php the_sub_field('link'); ?>" target="_blank">
                                        <div class="anchor-wrap">
                                            <?php svg_sub_embed('icon', 'social-icon'); ?>
                                            <div class="social-name small-title"><?php the_sub_field('name'); ?></div>
                                        </div>
                                    </a>
                        
                                <?php endwhile; ?>
                            </ul>
                        </div>
                        <div class="footer-contact">
                            <div class="footer-contact-inner">
                                <h3 class="contact-title">CONTACT</h3>

                                <?php
                                    $phone = get_field('phone_number', 'option');
                                    $phone = preg_replace('/\D+/', '', $phone);
                                ?>
                                <a href="tel:<?php echo $phone; ?>" class="footer-phone small-title"><?php the_field('phone_number', 'option'); ?></a>
                                <a class="footer-email small-title" href="mailto:info@animalcenter.org" target="_blank"><?php the_field('email_address', 'options'); ?></a>
                                <a class="footer-address small-title" href="<?php the_field('address_link', 'options') ?>"><?php the_field('address', 'options'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bottom-footer-wrap">
                    <div class="copyright-wrap">
                        <div class="source-org copyright text-center small-title white"><?php the_field('copyright_text', 'options'); ?></div>
                    </div>
                    
                    <div class="privacy-wrap">
                        <a class="privacy-policy" href="<?php the_field('privacy_link', 'options'); ?>"><?php the_field('privacy_policy', 'options'); ?></a>
                        <a class="terms-conditions" href="<?php the_field('terms_link', 'options'); ?>"><?php the_field('terms_&_conditions', 'options'); ?></a>
                    </div>
                </div>

            </footer>
                <?php wp_footer(); ?>
        </div> <?php //end .page-wrap ?>
	</body>
</html> <?php // end page. what a ride! ?>
