<?php $img = get_field('404_background_image', 'options'); ?>
<article id="post-not-found" class="post-not-found hentry text-center" style="background-image: url('<?php echo $img['url']; ?>');">
	<div class="post-not-found-inner">
		<header class="article-header">
				<h1><?php the_field('404_headline', 'options'); ?></h1>
		</header>
			<section class="entry-content">
				<p><?php the_field('404_content', 'options'); ?></p>
		</section>
		<footer class="article-footer">
				<a href="<?php bloginfo('url'); ?>" class="button button-bigger">Home</a>
		</footer>
	</div>
</article>