<div class="cf content-half-image-section-wrap

	<?php 

		if (get_sub_field('half_image_left_right') == "left"): 
			echo "content-half-image-content-left"; 
		elseif(get_sub_field('half_image_left_right') == "right"): 
			echo "content-half-image-content-right"; 
		endif; 

		$img = get_sub_field('half_image'); 

	?>">

	<div class="content-half-image sixcol first half-image">	
		<?php acf_sub_image('half_image') ?>
	</div>

	<div class="content-half-image-section-content sixcol last half-image">
		<div class="half-editor normal-page"><?php the_sub_field('only_half_image_editor'); ?></div>
	</div>

</div>