
<?php $img = get_sub_field('dark_block_image'); ?>

<div class="content-dark-block dark-block-responsive" style="background-image: url(<?php echo $img['url']; ?>);">
	<div class="cf dark-block-image">
		<h2 class="dark-block-headline h1"><?php the_sub_field('dark_block_headline'); ?></h2>
		<h2 class="text-section-one h2 "><?php the_sub_field('text_section_one'); ?></h2>
		<h2 class="text-section-two mid-title"><?php the_sub_field('text_section_two'); ?></h2>
		<h2 class="text-section-three h3 normal-page"><?php the_sub_field('text_section_three'); ?></h2>
		<?php $link = get_sub_field('dark_block_button_link'); ?>
		<?php  if (!empty($link)): ?>
			<a class="dark-block-button button arrow-button orange" href="<?php the_sub_field('dark_block_button_link') ?>"><?php tsf('dark_block_button'); ?></a>
		<?php endif; ?>
	</div>
</div>
