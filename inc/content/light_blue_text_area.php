<div class="cf light-blue-text-area <?php if(get_sub_field('full_width_section') == "Not Full Width"): echo "light-blue-text-area-not-full-width "; endif; ?> ">

  <?php if(get_sub_field('light_blue_column_option') == "two columns"): ?>
  	
  	<div class="p1 light-blue-editor-one sixcol first normal-page">

  		<?php tsf('light_blue_editor'); ?>

  	</div>

  	<div class="p1 light-blue-editor-two sixcol last normal-page">
    
  		<?php tsf('light_blue_editor_two'); ?>
  	
  	</div>

  <?php else: ?>

    <div class="p1 light-blue-editor mw-710 normal-page"><?php tsf('light_blue_editor'); ?></div>

  <?php endif; ?>
  
</div>