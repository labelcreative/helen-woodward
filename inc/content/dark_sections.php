<?php 
	$repeater = get_sub_field('dark_sections');
	$count = count($repeater);
?>
<div class="cf content-dark-section-wrap content-dark-section-responsive">

		<?php while (have_rows('dark_sections_repeater')): the_row(); ?>

			<?php $img = get_sub_field('dark_section_background_image'); ?>
			<div class="content-dark-section 

			<?php 

			if ($count == 1): echo "col-12"; 
			elseif ($count == 2): echo "col-6"; 
			elseif ($count == 3): echo "col-4"; 
			endif; ?>" style="background-image:url('<?php echo $img['url']; 

			?>');">>

		<div class="content-dark-section-inner">
			<h2 class="dark-section-title"><?php the_sub_field('dark_section_title') ?></h2>
			<p class="dark-section-editor normal-page"><?php the_sub_field('dark_section_editor'); ?></p> 
		</div>

	</div>

	<?php endwhile; ?>

</div>