<?php 
	$repeater = get_sub_field('flexible_image_content');
	$count = count($repeater);
?>
<div class="cf content-image-block-flexible-wrap image-block-responsive">

<?php while(have_rows('flexible_image_content')): the_row(); ?>

	
	<?php $img = get_sub_field('block_image'); ?>
	<div class="bgi content-image-block-flexible  <?php 
		if ($count == 1): 
			echo "col-12"; 
		elseif ($count == 2): 
			echo "col-6"; 
		elseif ($count > 3): 
			echo "col-4"; 
		endif; 
	?>" style="background-image:url('<?php echo $img['url']; ?>');">
	

		<div class="content-image-block-flexible-inner">
			<h2 class="block-title image-block white"><?php the_sub_field('block_title'); ?></h2>
			<p class="block-text image-block"><?php the_sub_field('block_text'); ?></p>
			<?php arrow_button(get_sub_field('block_link'), get_sub_field('block_button_text'), 'block-button-text white'); ?>
		</div>
	</div>

<?php endwhile; ?>

</div>