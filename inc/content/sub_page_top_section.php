<?php $img = get_sub_field('sub_page_top_section_image'); ?>

<div class="content-sub-page-top-section content-sub-page-top-section-responsive" style="background-image: url(<?php echo $img['url']; ?>);">
    <div class="sub-page-image">
		<div class="sub-page-text-sections">
			<h2 class="sub-text-section-one"><?php the_sub_field('sub_page_text_section_one'); ?></h2>
			<h2 class="sub-text-section-two"><?php the_sub_field('sub_page_text_section_two'); ?></h2>
			<h2 class="sub-text-section-three"><?php the_sub_field('sub_page_text_section_three'); ?></h2>
			<a class="sub-page-button" href="<?php the_sub_field('dark_block_button_link') ?>"><?php tsf('sub_page_top_section_button'); ?></a>
			<a class="sub-page-top-section-button" href="<?php the_sub_field('sub_page_top_section_button_link') ?>"><?php the_sub_field('sub_page_top_section_button') ?></a>
		</div>
   </div>
</div>

