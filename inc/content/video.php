<div class="content-video content-video-responsive">
	
	<?php $img = get_sub_field('video_photo'); ?>

	<div class="content-video-wrap">
		<div class="content-video-placeholder" style="background-image: url(<?php echo $img['url']; ?>);">
			<a class="video-photo video-player <?php if(get_sub_field('choose_video_type') == 'Vimeo'): echo "video-player--vimeo"; else: echo "video-player--youtube"; endif; ?>" href="#">
				<svg class="play-button-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				width="153.36px" height="153.36px" viewBox="0 0 153.36 153.36" enable-background="new 0 0 153.36 153.36" xml:space="preserve">
				<circle fill="none" stroke="#FFFFFF" stroke-width="2" stroke-miterlimit="10" cx="76.095" cy="70.394" r="62.247"/>
				<polygon fill="none" stroke="#FFFFFF" stroke-width="2" stroke-miterlimit="10" points="65.596,49.894 65.596,92.894 
				100.595,70.858 "/>
				</svg>
			</a>
		</div>
		<div class="content-video-embed">
			<div class="vidrap">
				<?php if(get_sub_field('choose_video_type') == 'Vimeo'): ?>
					<iframe src="https://player.vimeo.com/video/<?php the_sub_field('video_embed'); ?>?title=0&byline=0&portrait=0&api=1&player_id=hidden_vimeo" width="1400" height="787" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen id="hidden_vimeo"></iframe>
				<?php else: ?>
					<iframe width="1400" height="788" class="vid" id="hidden_youtube" src="https://www.youtube.com/embed/<?php the_sub_field('video_embed'); ?>?modestbranding=1&rel=0" frameborder="0" allowfullscreen></iframe>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="content-video-title-bar-color 
	<?php 
		if (get_sub_field('video_title_bar_color') == "orange"): 
			echo "video-orange"; 
		elseif(get_sub_field('video_title_bar_color') == "blue"): 
			echo "video-blue"; 
		elseif(get_sub_field('video_title_bar_color') == "black"): 
			echo "video-black"; 
		endif; 
	?>
	">
		<h3 class="video-title"><?php tsf('video_title'); ?></h3>

	</div>
</div>