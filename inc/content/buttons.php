
<div class="cf content-buttons text-center p1">
	<?php $i = 1; ?>
	<?php $count = count(get_sub_field('buttons_repeater')); ?>
	<?php while (have_rows('buttons_repeater')): the_row(); ?>

	<div class="p1 
		<?php 
			if ($count == 2): 
				echo " sixcol ";

				if ($i == 1): 
					echo " first "; 
				elseif ($i == 2): 
					echo " last "; 
				endif; 

			elseif ($count == 3):
				echo " fourcol ";

				if ($i == 1): 
					echo " first "; 
				elseif ($i == 3): 
					echo " last "; 
				
				endif; 
			endif;
		?>
	">
		<a class="button button-wide button-right-border content-button-text-colors-link <?php 

				if (get_sub_field('button_color') == 'blue'): 
						echo "blue";
				elseif (get_sub_field('button_color') == 'light blue'): 
						echo "blue-light"; 
				elseif (get_sub_field('button_color') == 'orange'): 
						echo "orange"; 
				endif;

			?>" href="<?php tsf('button_link'); ?>">

			<span><?php tsf('button_text'); ?></span>

		</a>
	</div>
	<?php $i++; ?>
	<?php endwhile; ?>

</div>
