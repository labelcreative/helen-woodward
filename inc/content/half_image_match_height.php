<div class="cf content-half-image-match-height-wrap <?php 

		if (get_sub_field('image_left_or_right') == "left"): 
			echo "content-half-image-match-height-content-left"; 
		elseif(get_sub_field('image_left_or_right') == "right"): 
			echo "content-half-image-match-height-content-right"; 
		endif;
		 
		?> 

		<?php

		if (get_sub_field('text_area_bg_color') == "blue"): 
			echo "content-half-image-match-height-content-bg-blue"; 
		elseif (get_sub_field('text_area_bg_color') == "white"): 
			echo "content-half-image-match-height-content-bg-white";
		endif; 

		$img = get_sub_field('full_height_image'); 
	?>">

	<div class="content-half-image-match-height-image matcher" style="background-image: url('<?php echo $img['url']; ?>');">	

	</div>

		<div class="content-half-image-match-height matcher">
			<div class="half-image-editor white normal-page"><?php the_sub_field('half_image_editor'); ?></div>
		</div>

</div>