<?php 
	$repeater = get_sub_field('small_block_flexible_content');
	$count = count($repeater);
?>
<div class="cf content-image-block-small-flexible-wrap image-block-small-responsive">

<?php while (have_rows('small_block_flexible_content')): the_row(); ?>


	<?php $img = get_sub_field('small_block_image'); ?>
	<div class="bgi content-image-block-small-flexible 

	<?php

	if ($count == 1): echo "col-12"; 
	elseif ($count == 2): echo "col-6"; 
	elseif ($count > 2): echo "col-4"; 
	endif; ?>" style="background-image:url('<?php echo $img['url']; ?>');">

	<div class="content-image-block-small-flexible-inner 

	<?php 
		
	if (get_sub_field('small_block_title_color') == "blue"): 
		echo "image-block-small-blue"; 
	elseif (get_sub_field('small_block_title_color') == "orange"): 
		echo "image-block-small-orange"; 
	endif; 

	?>

	">
		<h2 class="small-block-title image-block-small"><?php the_sub_field('small_block_title'); ?></h2>
		<?php 
			$text = get_sub_field('small_block_text');
			if (!empty($text)):
		?>
		<h3 class="small-block-text image-block-small white"><?php the_sub_field('small_block_text'); ?></h3>
		<?php endif; ?>
		<?php 
			$link = get_sub_field('small_block_button_text');
			if (!empty($link)):
				arrow_button(get_sub_field('small_block_link'), get_sub_field('small_block_button_text'), 'block-button-text white'); 
			endif;
		?>
	</div>

	</div>

<?php endwhile; ?>

</div>