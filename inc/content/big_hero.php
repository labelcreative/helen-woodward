<?php $img = get_sub_field('background_image'); ?>

<div class="content-big-hero" style="background-image: url('<?php echo $img['url']; ?>');">

	<h1 class="content-big-hero-title text-shadow h1"><?php the_sub_field('title'); ?></h1>

</div>


