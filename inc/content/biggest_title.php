	<div class="content-biggest-title">

		<h2 class="biggest-title

		<?php 

		if (get_sub_field('biggest_title_color') == "blue"): 
			echo "biggest-title-blue"; 
		elseif (get_sub_field('biggest_title_color') == "light-blue"): 
			echo "biggest-title-light-blue"; 
		elseif (get_sub_field('biggest_title_color') == "orange"): 
			echo "biggest-title-orange"; 
		endif;
		?>

		<?php

		if (get_sub_field('biggest_title_align') == "left"): 
			echo "biggest-title-left"; 
		elseif (get_sub_field('biggest_title_align') == "right"): 
			echo "biggest-title-right"; 
		elseif (get_sub_field('biggest_title_align') == "center"): 
			echo "biggest-title-center";
		endif; 
		?>">

		<?php the_sub_field('biggest_title_text'); ?>

		</h2>

	</div>

