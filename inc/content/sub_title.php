<div class="content-sub-title">
	<h2 class="sub-title

	<?php 
	
		// Color
		if (get_sub_field('sub_title_color') == "blue"): 
			echo "sub-title-blue"; 
		elseif (get_sub_field('sub_title_color') == "black"): 
			echo "sub-title-black"; 
		elseif (get_sub_field('sub_title_color') == "orange"): 
			echo "sub-title-orange"; 
		endif; 

		// Text Align
		if (get_sub_field('sub-align') == "left"): 
			echo "sub-title-left"; 
		elseif (get_sub_field('sub-align') == "right"): 
			echo "sub-title-right"; 
		elseif (get_sub_field('sub-align') == "center"): 
			echo "sub-title-center"; 
		endif; 

	?>">

		<?php the_sub_field('sub_title_header'); ?>

	</h2>
</div>
