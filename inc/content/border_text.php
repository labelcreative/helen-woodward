
	<div class="content-border-text

			<?php 

				// Border Color
				if (get_sub_field('border_color') == "blue"):
					echo "border-color-blue"; 

				elseif (get_sub_field('border_color') == "light blue"): 
					echo "border-color-light-blue"; 

				elseif (get_sub_field('border_color') == "orange"): 
					echo "border-color-orange"; 

				endif; 

			?> 
		
			<?php

				// Text Color
				if (get_sub_field('border_text_color') == "blue"):
					echo "border-text-blue";

				elseif (get_sub_field('border_text_color') == "light blue"): 
					echo "border-text-light-blue"; 

				elseif (get_sub_field('border_text_color') == "orange"): 
					echo "border-text-orange"; 

				endif; 

			?>">

		<!-- Border Text Editor -->
		<div class="border-text-editor content-border-text-responsive normal-page">

			<?php the_sub_field('border_text_editor') ?>

		</div>

	</div>
