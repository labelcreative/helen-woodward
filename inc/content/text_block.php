<div class="cf text-block">
  <?php if(get_sub_field('two_column_option') == "Two Column Option"): ?>
  	
  	<div class="p1 text-block-editor-one sixcol first normal-page">
  		<?php tsf('text_block_editor'); ?>
  	</div>
  	<div class="p1 text-block-editor-two sixcol last normal-page">
  		<?php tsf('text_block_editor_two'); ?>
  	
  	</div>
  <?php else: ?>

	<div class="p1 mw-960 text-block-editor-single normal-page"><?php tsf('text_block_editor'); ?></div>

  <?php endif; ?>
</div>

