
<div class="content-colored-bar-wrap <?php 

			// Full Width Option

			if (get_sub_field('colored_bar_full_width_option') == "Full Width"): 
				echo "content-colored-bar-full-width"; 
			elseif(get_sub_field('colored_bar_full_width_option') == "Not Full Width"): 
				echo "content-colored-bar-not-full-width"; 
		    endif;  ?> <?php

			// Color Option

			if (get_sub_field('bar_color') == 'orange'): 
				echo "colored-bar-orange"; 

			elseif (get_sub_field('bar_color') == 'blue'): 
				echo "colored-bar-blue"; 

			elseif (get_sub_field('bar_color') == 'light blue'): 
				echo "colored-bar-light-blue"; 
			endif;

	?>">

	<?php $headline = get_sub_field('colored_bar_headline'); 

	if( !empty($headline) ): ?>

		<h2 class="colored-bar-headline">
			
			<?php the_sub_field('colored_bar_headline'); ?>

		</h2>

	<?php endif; ?>


	<?php $button = get_sub_field('colored_bar_button');

	if( !empty($button) ): ?>

	  <div class="colored-bar-button-section">

		<a class="button button-right-border white" href="<?php the_sub_field('colored_bar_button_link') ?>" <?php if (get_sub_field('open_in_new_tab')): ?>target="_blank"<?php endif; ?>>

			<span><?php the_sub_field('colored_bar_button'); ?></span>

		</a>

	 </div>

	<?php endif; ?>

</div>