
	<div class="content-contact-area content-contact-area-responsive">
		
		<h2 class="contact-area-title">

		<?php the_sub_field('contact_area_title'); ?>

		</h2>
		
	<div class="cf contact-area-inner-content">

		<div class="contact-area-editor-one contact-area-left matcher normal-page">

		<?php the_sub_field('contact_area_editor_one'); ?>

		</div>

		<div class="contact-area-right matcher">

			<div class="contact-area-editor-two normal-page">

				<?php the_sub_field('contact_area_editor_two'); ?>

			</div>

			<div class="p1 text-center">
			
				<a class="button" href="<?php the_sub_field('contact_area_button_link') ?>" target="_blank"><?php the_sub_field('contact_area_button'); ?></a>

			</div>

		</div>

	</div>

	</div>

