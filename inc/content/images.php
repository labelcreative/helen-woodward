<?php $i = 1; ?>
<?php $count = count(get_sub_field('images')); ?>

<div class="cf p1 content-images-wrap">
  <?php while (have_rows('images')): the_row(); ?>
    <div class="p1 
      <?php 
        if ($count == 2): 
          echo " sixcol ";

          if ($i == 1): 
            echo " first "; 
          elseif ($i == 2): 
            echo " last "; 
          endif; 

        elseif ($count == 3):
          echo " fourcol ";

          if ($i == 1): 
            echo " first "; 
          elseif ($i == 3): 
            echo " last "; 
          
          endif; 
        endif;
      ?>
    ">
    <?php acf_sub_image('image'); ?>
    </div>
  <?php endwhile; ?>
</div>

