<div class="content-slider content-slider-responsive">
	
	<ul class="content-inner-slider-list o-slider" id="slider">

		<?php while(have_rows('slider_photos_repeater')): the_row(); ?>

			<?php $img = get_sub_field('slider_image'); ?>

			<li>

				<img src="<?php echo $img['url']; ?>">

			</li>

		<?php endwhile; ?>

	</ul>

	<div class="slider-title-bar-color

	<?php 

		if (get_sub_field('slider_title_bar_color') == 'orange'): 
			echo "slider-orange"; 

		elseif (get_sub_field('slider_title_bar_color') == 'blue'): 
			echo "slider-blue"; 

		elseif (get_sub_field('slider_title_bar_color') == 'black'): 
			echo "slider-black"; 
		
		endif;

	?>">
		<h3 class="slider-text"><?php the_sub_field('slider_text') ?></h3>
		
	</div> 

</div>
