<div class="content-big-title">

	<h2 class="big-title <?php 
	if (get_sub_field('big_title_color') == "blue"): 
		echo "big-title-blue"; 
	elseif (get_sub_field('big_title_color') == "light blue"): 
		echo "big-title-light-blue"; 
	elseif (get_sub_field('big_title_color') == "orange"): 
		echo "big-title-orange"; 
	endif; ?> 

	<?php 
	if (get_sub_field('big_title_align') == "left"): 
		echo "big-title-left"; 
	elseif (get_sub_field('big_title_align') == "right"): 
		echo "big-title-right"; 
	elseif (get_sub_field('big_title_align') == "center"): 
		echo "big-title-center"; 
	endif; 

	?>">

	<?php the_sub_field('big_title_header'); ?>

	</h2>

</div>

