<?php $img = get_sub_field('bg_background_image'); ?>

<div class="content-bg-image-text <?php 
		if (get_sub_field('bg_image_background_color') == "blue"):
			echo "content-bg-image-blue"; 
		elseif (get_sub_field('bg_image_background_color') == "white"): 
			echo "content-bg-image-white"; 
		endif; 

	?>" style="background-image: url('<?php echo $img['url']; ?>');">
	
	<div class="mw-710 bg-image-text-editor normal-page">
	<?php the_sub_field('bg_image_text_editor')?>
	</div>

</div>