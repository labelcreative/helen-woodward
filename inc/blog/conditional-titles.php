<?php if (is_category()) { ?>
	<h1 class="archive-title mt0">
		<span><?php _e( 'Category:', 'labeltheme' ); ?></span> <?php single_cat_title(); ?>
	</h1>

<?php } elseif (is_tag()) { ?>
	<h1 class="archive-title mt0">
		<span><?php _e( 'Tag:', 'labeltheme' ); ?></span> <?php single_tag_title(); ?>
	</h1>

<?php } elseif (is_author()) {
	global $post;
	$author_id = $post->post_author;
?>
	<h1 class="archive-title mt0">

		<span><?php _e( 'Author:', 'labeltheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>

	</h1>
<?php } elseif (is_day()) { ?>
	<h1 class="archive-title mt0">
		<span><?php _e( 'Archives:', 'labeltheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
	</h1>

<?php } elseif (is_month()) { ?>
		<h1 class="archive-title mt0">
			<span><?php _e( 'Archives:', 'labeltheme' ); ?></span> <?php the_time('F Y'); ?>
		</h1>

<?php } elseif (is_year()) { ?>
		<h1 class="archive-title mt0">
			<span><?php _e( 'Archives:', 'labeltheme' ); ?></span> <?php the_time('Y'); ?>
		</h1>
<?php } elseif (!empty($search)) { ?>
		<h1 class="archive-title lh text-center">
			<span><?php _e( 'Search Results for:', 'labeltheme' ); ?></span> <?php echo esc_attr(get_search_query()); ?>
		</h1>
<?php } ?>
<?php
if(is_archive()){
	$cat = get_category( get_query_var( 'cat' ) );
	$cat_slug = $cat->slug;
}
?>
<?php if (is_search()) {
	$search = get_search_query();
} ?>
<?php if (is_author()) {
	$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
	$author = $curauth->ID;
} ?>