<footer class="article-footer">
	
	<div class="cf share-article">
		<a class="share-button share-button-tw" href="#" target="_blank">Tweet</a>
		<a class="share-button share-button-fb" href="#" target="_blank">Share</a>
 	</div>

	
<script>
	jQuery(document).ready(function($) {
		$(function() {
		    $('.share-button-fb').on('click', function() {
		        var w = 580, h = 300,
		                left = (screen.width/2)-(w/2),
		                top = (screen.height/2)-(h/2);
		            
		            if ((screen.width < 480) || (screen.height < 480)) {
		                window.open ('http://www.facebook.com/share.php?u=<?php the_permalink(); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
		            } else {
		                window.open ('http://www.facebook.com/share.php?u=<?php the_permalink(); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);   
		            }
		    });
		    $('.share-button-tw').on('click', function() {
		        var loc = encodeURIComponent('<?php the_permalink(); ?>'),
		                title = "<?php the_title(); ?>",
		                w = 580, h = 300,
		                left = (screen.width/2)-(w/2),
		                top = (screen.height/2)-(h/2);
		                
		            window.open('http://twitter.com/share?text=' + title + '&url=' + loc, '', 'height=' + h + ', width=' + w + ', top='+top +', left='+ left +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
		    });
		});
	});

</script>
</footer><?php // end article footer ?>