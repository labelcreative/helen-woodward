<?php 
	while (have_rows('content')): the_row();
	
		 if( get_row_layout() == 'big_hero' ):
	
	    	get_template_part( 'inc/content/big_hero');
	
	    elseif( get_row_layout() == 'text_block' ): 
	
	    	get_template_part( 'inc/content/text_block');
		
		elseif( get_row_layout() == 'divider' ): 
	
	    	get_template_part( 'inc/content/divider');
	
	    elseif( get_row_layout() == 'space' ): 
	
	    	get_template_part( 'inc/content/space');

	    elseif( get_row_layout() == 'biggest_title' ): 
	
	    	get_template_part( 'inc/content/biggest_title');

	    elseif( get_row_layout() == 'big_title' ): 
	
	    	get_template_part( 'inc/content/big_title');

	    elseif( get_row_layout() == 'sub_title' ): 
	
	    	get_template_part( 'inc/content/sub_title');

	    elseif( get_row_layout() == 'image_block' ): 
	
	    	get_template_part( 'inc/content/image_block');

	    elseif( get_row_layout() == 'image_block_small' ): 
	
	    	get_template_part( 'inc/content/image_block_small');

	    elseif( get_row_layout() == 'half_image_match_height' ): 
	
	    	get_template_part( 'inc/content/half_image_match_height');

	    elseif( get_row_layout() == 'half_image_section' ): 
	
	    	get_template_part( 'inc/content/half_image');

	    elseif( get_row_layout() == 'dark_block' ): 
	
	    	get_template_part( 'inc/content/dark_block');

	    elseif( get_row_layout() == 'contact_area' ): 
	
	    	get_template_part( 'inc/content/contact_area');

	    elseif( get_row_layout() == 'sub_page_top_section' ): 
	
	    	get_template_part( 'inc/content/sub_page_top_section');

	    elseif( get_row_layout() == 'colored_bar' ): 
	
	    	get_template_part( 'inc/content/colored_bar');

	    elseif( get_row_layout() == 'light_blue_text_area' ): 
	
	    	get_template_part( 'inc/content/light_blue_text_area');

	    elseif( get_row_layout() == 'light_blue_text_area_full_width' ): 
	
	    	get_template_part( 'inc/content/light_blue_text_area_full_width');

	    elseif( get_row_layout() == 'slider' ): 
	
	    	get_template_part( 'inc/content/slider');

	    elseif( get_row_layout() == 'video' ): 
	
	    	get_template_part( 'inc/content/video');

	    elseif( get_row_layout() == 'bg_image_text' ): 
	
	    	get_template_part( 'inc/content/bg_image_text');

	    elseif( get_row_layout() == 'buttons' ): 
	
	    	get_template_part( 'inc/content/buttons');

	    elseif( get_row_layout() == 'border_text' ): 
	
	    	get_template_part( 'inc/content/border_text');

	    elseif( get_row_layout() == 'dark_sections' ): 
	
	    	get_template_part( 'inc/content/dark_sections');

	    elseif( get_row_layout() == 'images' ): 
	
	    	get_template_part( 'inc/content/images');

	    endif;
	
	endwhile; 
?>