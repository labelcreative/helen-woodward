<div class="bgcfff" id="single-content">
	<?php $img = get_field('top_background_image', 'options'); ?>
	<div class="top-litter-content" style="background-image: url('<?php echo $img['url']; ?>');">
	  <div class="little-content-inner">
		<h1 class="cf extra-large-title litter-title">
			<?php 
				if (is_singular('success_story')):
					the_field('success_headline', 'options');
				elseif (is_singular('litter')):
			?>
				<div class="mw-960">
					<span class="litter-title-twitter-icon"><?php svg_options_embed('litter_icon'); ?></span><span><div data-fitter-happier-text><?php the_field('litter_headline', 'options'); ?></div></span>
				</div>
			<?php
				endif;
			?>
		</h1>
	  </div>
	</div>
	<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<?php 
			if ( function_exists('yoast_breadcrumb') ):
				yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			endif;
		?>
	</div>
</div>
<?php if (is_singular('litter')): ?>
	<div class="cf follow-the-litter-featured">
		<?php $img = get_field('image'); ?>
		<div class="follow-the-litter-featured-image matcher" style="background-image: url('<?php echo $img['url']; ?>');"></div>
		<div class="follow-the-litter-featured-content matcher">
			<div class="follow-the-litter-featured-content-inner">
				<h2 class="ttu white"><?php the_title(); ?></h2>
				<div class="follow-the-litter-featured-description white"><?php tf('description'); ?></div>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="mw-1140">
	<div class="cf">
		<div class="p1 text-center">
		</div>
	</div>
	<div class="single-success-story-content">
		<div class="p1 single-success-story-sidebar threecol first">
			<div class="mb05">
				<a href="<?php echo get_permalink(394); ?>" class="roller button button-right-border orange"><span>ADOPT FROM THIS 
				<?php 
					if (is_singular('success_story')):
						echo 'STORY';
					elseif (is_singular('litter')):
						echo 'LITTER';
					endif;
				?>
				</span></a>
			</div>
			<a class="button button-right-border blue-light button-wide share-tw" href="#" target="_blank"><span>Tweet</span></a>
			<a class="button button-right-border blue button-wide share-fb" href="#" target="_blank"><span>Share</span></a>
		</div>
		<div class="single-article-bottom-wrap ninecol last">
			<div class="normal-page p1 pt0"><?php the_content(); ?></div>
			<div class="p1 cf">
				<div class="single-article-bottom-inner flr text-right">
					<div>
						<a href="#single-content" class="roller button button-right-border blue button-wide"><span>Back to top</span></a>
					</div>
					<div>
						<a href="
						<?php 
							if (is_singular('success_story')):
								echo get_permalink(400);
							elseif (is_singular('litter')):
								echo get_permalink(519);
							endif;
						?>
						" class="button button-thick-right  button-right-border orange button-wide"><span>
						<?php 
							if (is_singular('success_story')):
								echo 'See More Stories';
							elseif (is_singular('litter')):
								echo 'Follow More Litters';
							endif;
						?>
						</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	jQuery(document).ready(function($) {
		$(function() {
		    $('.share-fb').on('click', function() {
		        var w = 580, h = 300,
		                left = (screen.width/2)-(w/2),
		                top = (screen.height/2)-(h/2);
		            
		            if ((screen.width < 480) || (screen.height < 480)) {
		                window.open ('http://www.facebook.com/share.php?u=<?php the_permalink(); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
		            } else {
		                window.open ('http://www.facebook.com/share.php?u=<?php the_permalink(); ?>', '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);   
		            }
		    });
		    $('.share-tw').on('click', function() {
		        var loc = encodeURIComponent('<?php the_permalink(); ?>'),
		                title = "<?php the_title(); ?>",
		                w = 580, h = 300,
		                left = (screen.width/2)-(w/2),
		                top = (screen.height/2)-(h/2);
		                
		            window.open('http://twitter.com/share?text=' + title + '&url=' + loc, '', 'height=' + h + ', width=' + w + ', top='+top +', left='+ left +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
		    });
		});
	});

</script>