<?php get_header(); ?>

<!-- header -->
<div class="page-header-wrap">
	<h1 class="page-header"><?php the_title(); ?></h1>
</div>
<!-- breadcrumbs -->
<div class="breadcrumbs">
	<?php 
	//if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<p id="breadcrumbs">','</p>');
	//} 
	?>
</div>

<div class="page-content normal-page p1 mw-710"><?php the_content(); ?></div>

<?php get_footer(); ?>
