<?php
/* 
Custom Post Type -- Success
*/

// Flush rewrite rules for success story types
add_action( 'after_switch_theme', 'label_flush_rewrite_rules' );

// Flush your rewrite rules (can only happen once (if using more than 1 cpt delete this))
function label_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the success story
function success_story() { 
	// creating (registering) the success story 
	register_post_type( 'success_story', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Success Stories', 'labeltheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Success Story', 'labeltheme' ), /* This is the individual type */
			'all_items' => __( 'All Success Stories', 'labeltheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'labeltheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Success Story', 'labeltheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'labeltheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Success Stories', 'labeltheme' ), /* Edit Display Title */
			'new_item' => __( 'New Success Story', 'labeltheme' ), /* New Display Title */
			'view_item' => __( 'View Success Story', 'labeltheme' ), /* View Display Title */
			'search_items' => __( 'Search Success Story', 'labeltheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'labeltheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'labeltheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the success stories', 'labeltheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-lightbulb', /* the icon for the success story type menu */
			'rewrite'	=> array( 'slug' => 'adopt-a-pet/success-stories', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail')
	 	) /* end of options */
	); /* end of register post type */
	
	
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'success_story');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// //  add custom categories (these act like categories)
 //    register_taxonomy( 'custom_cat', 
 //    	array('success_story'), /* if you change the name of register_post_type( 'success_story', then you have to change this */
 //    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
 //    		'labels' => array(
 //    			'name' => __( 'Custom Categories', 'labeltheme' ), /* name of the custom taxonomy */
 //    			'singular_name' => __( 'Custom Category', 'labeltheme' ), /* single taxonomy name */
 //    			'search_items' =>  __( 'Search Custom Categories', 'labeltheme' ), /* search title for taxomony */
 //    			'all_items' => __( 'All Custom Categories', 'labeltheme' ), /* all title for taxonomies */
 //    			'parent_item' => __( 'Parent Custom Category', 'labeltheme' ), /* parent title for taxonomy */
 //    			'parent_item_colon' => __( 'Parent Custom Category:', 'labeltheme' ), /* parent taxonomy title */
 //    			'edit_item' => __( 'Edit Custom Category', 'labeltheme' ), /* edit custom taxonomy title */
 //    			'update_item' => __( 'Update Custom Category', 'labeltheme' ), /* update title for taxonomy */
 //    			'add_new_item' => __( 'Add New Custom Category', 'labeltheme' ), /* add new title for taxonomy */
 //    			'new_item_name' => __( 'New Custom Category Name', 'labeltheme' ) /* name title for taxonomy */
 //    		),
 //    		'show_admin_column' => true, 
 //    		'show_ui' => true,
 //    		'query_var' => true,
 //    		'rewrite' => array( 'slug' => 'custom-slug' ),
 //    	)
 //    );   
    
	// //  add custom tags (these act like categories)
 //    register_taxonomy( 'custom_tag', 
 //    	array('success_story'), /* if you change the name of register_post_type( 'success_story', then you have to change this */
 //    	array('hierarchical' => false,    /* if this is false, it acts like tags */                
 //    		'labels' => array(
 //    			'name' => __( 'Custom Tags', 'labeltheme' ), /* name of the custom taxonomy */
 //    			'singular_name' => __( 'Custom Tag', 'labeltheme' ), /* single taxonomy name */
 //    			'search_items' =>  __( 'Search Custom Tags', 'labeltheme' ), /* search title for taxomony */
 //    			'all_items' => __( 'All Custom Tags', 'labeltheme' ), /* all title for taxonomies */
 //    			'parent_item' => __( 'Parent Custom Tag', 'labeltheme' ), /* parent title for taxonomy */
 //    			'parent_item_colon' => __( 'Parent Custom Tag:', 'labeltheme' ), /* parent taxonomy title */
 //    			'edit_item' => __( 'Edit Custom Tag', 'labeltheme' ), /* edit custom taxonomy title */
 //    			'update_item' => __( 'Update Custom Tag', 'labeltheme' ), /* update title for taxonomy */
 //    			'add_new_item' => __( 'Add New Custom Tag', 'labeltheme' ), /* add new title for taxonomy */
 //    			'new_item_name' => __( 'New Custom Tag Name', 'labeltheme' ) /* name title for taxonomy */
 //    		),
 //    		'show_admin_column' => true,
 //    		'show_ui' => true,
 //    		'query_var' => true,
 //    	)
 //    ); 
    
   
?>
