<?php 

function remove_acf_menu(){
    $admins = array( // provide a list of usernames who can edit custom field definitions here
        'aquaman'
    );
    $current_user = wp_get_current_user();// get the current user
    if( !in_array( $current_user->user_login, $admins ) )// match and remove if needed
	    {
	        remove_menu_page('edit.php?post_type=acf');
	    }
}
 
add_action( 'admin_menu', 'remove_acf_menu', 999 );


// add options page for acf
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}


// change search form
function html5_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="cf searchform" action="' . home_url( '/blog/' ) . '" >
    <input type="search" placeholder="'.__("Search our blog").'" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="Search" />
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'html5_search_form' );

// acf image getter
// ex: acf_image('top_image');
function acf_image($field) {
   $img = get_field($field);
   $title = get_the_title();
   echo '<img src="'. $img['url'] .'" title="'. $title .'">';
}
function acf_sub_image($field) {
   $img = get_sub_field($field);
   $title = get_the_title();
   echo '<img src="'. $img['url'] .'" title="'. $title .'">';
}
function svg_embed($field, $class_name="svg-wrap"){
	if ( get_field($field) ) :
	  $svg = get_field($field);
	  if ( strpos( $svg, '.svg' ) !== false ) :
	    $svg = str_replace( site_url(), '', $svg);
	    print '<div class="wrap-svg '. $class_name .'">';
	    	echo file_get_contents(ABSPATH . $svg);
	    print '</div>';
	   endif;
	endif; 
}
function svg_options_embed($field){
	if ( get_field($field, 'options') ) :
	  $svg = get_field($field, 'options');
	  if ( strpos( $svg, '.svg' ) !== false ) :
	    $svg = str_replace( site_url(), '', $svg);
	    	echo file_get_contents(ABSPATH . $svg);
	   endif;
	endif; 
}
function svg_sub_embed($field, $class_name){
	if ( get_sub_field($field) ) :
	  $svg = get_sub_field($field);
	  if ( strpos( $svg, '.svg' ) !== false ) :
	    $svg = str_replace( site_url(), '', $svg);
	    print '<div class="wrap-svg '. $class_name .'">';
	    	echo file_get_contents(ABSPATH . $svg);
	    print '</div>';
	   endif;
	endif; 
}
function tf($value){
	the_field($value);
}
function tfo($value){
	the_field($value, 'options');
}
function tsf($value){
	the_sub_field($value);
}



// add svgs to media uploader
function svg_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'svg_mime_types' ); 

// control svg size in image acf
function svg_size() { 
  echo '<style> 
    svg, img[src*=".svg"] { 
      max-width: 150px !important; 
      max-height: 150px !important; 
    }
  </style>'; 
}
add_action('admin_head', 'svg_size');


?>