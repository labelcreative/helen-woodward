<?php
/* 
Custom Post Type -- Litter
*/


// let's create the function for the litter story
function litter() { 
	// creating (registering) the litter story 
	register_post_type( 'litter', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Litters', 'labeltheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Litter', 'labeltheme' ), /* This is the individual type */
			'all_items' => __( 'All Litters', 'labeltheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'labeltheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Litter', 'labeltheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'labeltheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Litters', 'labeltheme' ), /* Edit Display Title */
			'new_item' => __( 'New Litter', 'labeltheme' ), /* New Display Title */
			'view_item' => __( 'View Litter', 'labeltheme' ), /* View Display Title */
			'search_items' => __( 'Search Litter', 'labeltheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'labeltheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'labeltheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'These are the litters', 'labeltheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-smiley', /* the icon for the litter story type menu */
			'rewrite'	=> array( 'slug' => 'litters', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail')
	 	) /* end of options */
	); /* end of register post type */
	
	
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'litter');
	   
?>
